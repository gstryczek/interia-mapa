import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducers'
import createGoogleAnaliticsMiddleware from './utils/googleMiddleware'

import { SELECT, DESELECT } from './reducers/selectedShop'
import { getSelectedShop } from './reducers/selectors'

const googleAnaliticsMiddleware = createGoogleAnaliticsMiddleware({
  [SELECT]: (action, state) => {
    const selectedShop = getSelectedShop(state)
    return {
      category: 'Sklep',
      action: 'Wybranie',
      label: `${selectedShop.siec} - ${selectedShop.adres}`
    }
  },
  [DESELECT]: () => ({
    category: 'Sklep',
    action: 'Zamknięcie'
  }),
})

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunk,
    googleAnaliticsMiddleware
  )
)

export default store
