import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import store from './store'
import './index.css'
import { fetchShops } from './reducers/shops'
import './vendor.js'

// fetch data
store.dispatch(fetchShops())


const MOUNT_NODE = document.getElementById('root')

ReactDOM.render(
  <App store={store} />
  , MOUNT_NODE
)
