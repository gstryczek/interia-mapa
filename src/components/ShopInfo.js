import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, css } from 'aphrodite'
import moment from 'moment'
import { ucFirst } from '../utils/string'
import { first } from 'lodash'

const ShopInfo = ({ shop }) => shop ? (
  <div className={css(ShopInfoStyleSheet.container)}>
    <div>
      <h1 className={css(ShopInfoStyleSheet.header)}>
        {shop.logo && <img className={css(ShopInfoStyleSheet.logo)} src={shop.logo} alt={shop.siec} />}
        {shop.siec}
      </h1>
      <p>{shop.adres}</p>
      <ul className={css(ShopInfoStyleSheet.list)}>
        {weekOrder.map((day, k) => (
          <li key={day} className={css(
            ShopInfoStyleSheet.day,
            moment().day() === k ? ShopInfoStyleSheet.today : null
          )}>
            <span>{ucFirst(moment().day(k).format('dddd'))}:</span>
            <span className={css(ShopInfoStyleSheet.hours)}>{first(shop.godziny)[day]}</span>
          </li>
        ))}
      </ul>
    </div>
  </div>
) : null

const weekOrder = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
]

const ShopInfoStyleSheet = StyleSheet.create({
  container: {
    width: '300px',
    padding: '0 15px'
  },
  hours: {
    float: 'right',
  },
  day: {
    margin: '3px 0'
  },
  header: {
    fontWeight: 'bold',
    display: 'flex',
    alignItems: 'center',
    marginBottom: '20px',
  },
  logo: {
    maxWidth: '100px',
    maxHeight: '100px',
    marginRight: '10px',
  },
  list: {
    marginTop: '20px'
  },
  today: {
    fontWeight: 'bold',
    background: '#f0f0f0'
  },
})

ShopInfo.propTypes = {
  shop: PropTypes.object,
}

export default ShopInfo
