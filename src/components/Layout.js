import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import ShopMap from '../containers/ShopMap'
import ShopList from '../containers/ShopList'

export default ({ menuOpen }) => (
  <div className={css(AppStyleSheet.wrapper)}>
    <div className={(css(AppStyleSheet.panel))}>
      <ShopList />
    </div>
    <ShopMap />
  </div>
)

export const AppStyleSheet = StyleSheet.create({
  wrapper: {
    width: '100vw',
    height: '100vh',
    overflow: 'hidden',
    display: 'flex',
    position: 'relative',
    '@media ( max-width: 600px )': {
      flex: 1,
      minHeight: '200px',
      flexDirection: 'column-reverse'
    }
  },
  panel: {
    minWidth: '380px',
    minHeight: '200px',
    background: '#cfcfef',
    zIndex: 100,
    overflow: 'auto',
  }
})
