import React, { Component } from 'react'


class MapDiv extends Component {
  shouldComponentUpdate() {
    return false
  }
  render() {
    return <div ref={this.props.registerRef} style={this.props.style} />
  }
}

MapDiv.defaultProps = {
  style: {
    width: '100%',
    height: '100%',
  }
}

export default MapDiv
