import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { renderToString } from 'react-dom/server'

class InfoWindow extends Component {

  isVisible = false

  render() {
    return null
  }
  componentDidMount() {
    const { maps } = this.props
    this.infoWindow = new maps.InfoWindow({})
    this.update()
  }
  bindOnCloseClickListener() {
    if (this.closeClickEventId) {
      this.props.maps.event.removeListener(this.closeClickEventId)
    }
    if (this.props.onCloseClick) {
      this.closeClickEventId = this.infoWindow.addListener(
        'closeclick', this.props.onCloseClick
      )
    }
  }

  componentDidUpdate() {
    this.update()
  }

  componentWillUnmount() {
    this.infoWindow.close()
  }

  update() {
    const { isOpen, position, map } = this.props
    const { infoWindow, isVisible } = this

    if (isOpen !== isVisible) {
      this.isVisible = isOpen
      if (isOpen) {
        infoWindow.open(map)
      } else {
        infoWindow.close()
      }
    }

    infoWindow.setContent(this.getContent())
    if (position.lng != null && position.lat != null) {
      infoWindow.setPosition(position)
    }
    this.bindOnCloseClickListener()
  }
  getContent() {
    const { children } = this.props
    const child = React.Children.only(children)
    const content = renderToString(child)

    return content
  }
}

InfoWindow.propTypes = {
  maps: PropTypes.object,
  map: PropTypes.object,
  position: PropTypes.object,
  isOpen: PropTypes.bool,
  onCloseClick: PropTypes.func,
}

export default InfoWindow
