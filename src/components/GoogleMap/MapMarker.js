import { Component } from 'react'
import PropTypes from 'prop-types'

class MapMarker extends Component {
  render() {
    return null
  }
  componentDidMount() {
    const { map, maps, lat, lng, iconUrl, iconSize, onClick } = this.props

    const icon = iconUrl ? {
      url: iconUrl,
      scaledSize: new maps.Size(iconSize, iconSize)
    } : null

    const marker = new maps.Marker({
      position: {
        lat,
        lng
      },
      icon,
      map,
    })

    if (iconUrl) {
      const img = new Image()
      img.onload = () => {
        const { height, width } = img
        const ratio = height / width

        marker.setIcon({
          ...icon,
          scaledSize: new maps.Size(iconSize, iconSize * ratio)
        })
      }
      img.src = iconUrl
    }

    marker.addListener('click', (e) => {
      map.setZoom(18)
      map.setCenter(marker.getPosition())
      if (onClick) {
        onClick()
      }
    })

    this.marker = marker
  }

  componentWillUnmount() {
    this.marker.setMap(null)
  }
}

MapMarker.defaultProps = {
  iconUrl: null,
  title: ''
}

MapMarker.propTypes = {
  maps: PropTypes.object,
  map: PropTypes.object,
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired,
  iconUrl: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  iconSize: PropTypes.number,
}

export default MapMarker
