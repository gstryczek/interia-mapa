import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MapDiv from './MapDiv'

class GoogleMap extends Component {

  componentDidMount() {
    const { maps } = global.google
    const { startLocation, initialZoom, useGeolocation, disableDefaultUI } = this.props
    this.maps = maps

    this.map = new maps.Map(this.mapDOM, {
      center: startLocation,
      zoom: initialZoom,
      disableDefaultUI
    })

    // user geolocation
    if (useGeolocation && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        }

        this.map.setCenter(pos)
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    const { lat: nextLat, lng: nextLng } = nextProps.center
    const { lat, lng } = this.props

    if (nextLat !== lat || nextLng !== lng) {
      this.map.setCenter(nextProps.center)
      this.map.setZoom(18)
    }
  }

  renderChildren() {
    const { children } = this.props

    if (!this.maps)
      return

    return React.Children.map(children, (child) => React.cloneElement(child, {
      map: this.map,
      maps: this.maps,
      iconSize: this.props.iconSize
    }))
  }

  registerRef = ref => this.mapDOM = ref

  render() {
    const { width, height } = this.props

    return <div style={{ width, height }}>
      <MapDiv registerRef={this.registerRef} />
      {this.renderChildren()}
    </div>
  }
}

GoogleMap.defaultProps = {
  startLocation: {
    lat: 52.23,
    lng: 21,
  },
  initialZoom: 12,
  iconSize: 64,
  width: '500px',
  height: '400px',
  center: {},
  useGeolocation: false,
  disableDefaultUI: false,
}

GoogleMap.propTypes = {
  useGeolocation: PropTypes.bool,
  startLocation: PropTypes.object,
  initialZoom: PropTypes.number,
  iconSize: PropTypes.number,
  width: PropTypes.string,
  height: PropTypes.string,
  center: PropTypes.object,
  disableDefaultUI: PropTypes.bool,
}

export default GoogleMap
