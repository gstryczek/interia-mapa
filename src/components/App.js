import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import ReactGA from 'react-ga'
import Layout from './Layout'
import { GAKey } from '../constants'


class App extends Component {
  render() {
    const { store } = this.props
    return (
      <Provider store={store}>
        <Layout />
      </Provider>
    )
  }
  componentDidMount(){
    ReactGA.initialize(GAKey)
  }
}

App.propTypes = {
  store: PropTypes.object
}

export default App
