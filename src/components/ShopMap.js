import React from 'react'
import PropTypes from 'prop-types'
import { default as GoogleMap, MapMarker, InfoWindow } from './GoogleMap'
import ShopInfo from './ShopInfo'
import textToImg from '../utils/textToImg'


const ShopMap = ({ shops, selected, selectShop, deselectShop }) => {
  const selectedPosition = selected ?
    {
      lat: Number(selected.latitude),
      lng: Number(selected.longitude)
    } : {}

  return (
    <GoogleMap
      width={'100%'}
      height={'100%'}
      center={selectedPosition}
      disableDefaultUI
    >
      {shops.map(shop => <MapMarker
        key={shop.id}
        lat={Number(shop.latitude)}
        lng={Number(shop.longitude)}
        iconUrl={shop.logo || textToImg(shop.siec)}
        onClick={() => { selectShop(shop.id) }}
      />)}
      <InfoWindow
        position={selectedPosition}
        isOpen={!!selected}
        onCloseClick={deselectShop}
      >
        <ShopInfo shop={selected} />
      </InfoWindow>
    </GoogleMap>
  )
}

ShopMap.propTypes = {
  shops: PropTypes.array.isRequired,
  selected: PropTypes.object
}

export default ShopMap
