import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, css } from 'aphrodite'

const List = ({ shops, selected, select }) => (
  <ul className={css(ListStyles.list)}>
    {shops.map(shop => (<li
      key={shop.id}

      className={css(
        ItemStyles.listItem,
        selected === shop && ItemStyles.selected
      )}
      onClick={() => { select(shop.id) }}
    >
      <i className={css(ItemStyles.icon) + ' fa fa-map-pin'} aria-hidden="true"></i>
      {shop.adres}
    </li>))}
  </ul>
)

List.defaultProps = {
  shops: [],
  selected: null,
}

List.propTypes = {
  shops: PropTypes.arrayOf(PropTypes.object).isRequired,
  selected: PropTypes.object,
  select: PropTypes.func,
}

const ListStyles = StyleSheet.create({
  list: {
    listStyle: 'none',
  },
})

const ItemStyles = StyleSheet.create({
  icon: {
    margin: '0 10px'
  },
  listItem: {
    width: '100%',
    cursor: 'pointer',
    padding: '10px 5px',
    borderBottom: '2px solid #ddd',
    ':hover': {
      backgroundColor: '#ddd'
    },
  },
  selected: {
    background: '#ddd'
  }
})

export default List
