import { connect } from 'react-redux'
import List from '../components/List'
import { selectShop } from '../reducers/selectedShop'
import { getSelectedShop, getShops } from '../reducers/selectors'

const mapStateToProps = state => {
  return {
    shops: getShops(state),
    selected: getSelectedShop(state)
  }
}

const mapDispatchToProps = dispatch => ({
  select: id => dispatch(selectShop(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(List)