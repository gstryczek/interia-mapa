import { connect } from 'react-redux'
import ShopMap from '../components/ShopMap'
import { getSelectedShop, getShops } from '../reducers/selectors'
import { selectShop, deselectShop } from '../reducers/selectedShop'

const mapStateToProps = state => ({
  shops: getShops(state),
  selected: getSelectedShop(state)
})

const mapDispatchToProps = dispatch => ({
  selectShop: id => dispatch(selectShop(id)),
  deselectShop: () => dispatch(deselectShop())
})

export default connect(mapStateToProps, mapDispatchToProps)(ShopMap)
