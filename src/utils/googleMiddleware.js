import ReactGA from 'react-ga'

export default actionLoggers =>
  store => next => action => {
    next(action)

    if (Object.keys(actionLoggers).includes(action.type)) {

      const eventData = actionLoggers[action.type](action, store.getState())
      
      ReactGA.event(eventData)
    }
  }
