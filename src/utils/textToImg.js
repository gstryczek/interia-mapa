export default (text) => {
  const canvas = document.createElement("canvas")

  

  canvas.width = 300
  canvas.height = 150
  const { width, height } = canvas

  const ctx = canvas.getContext('2d')

  // background
  ctx.fillStyle = '#fff'
  ctx.fillRect(0,0, canvas.width, canvas.height)

  // text
  ctx.fillStyle = '#000'
  ctx.font = "50pt Arial"
  ctx.fillText(text, 20, 100)

  // border
  ctx.lineWidth = 5
  ctx.fillStyle = "#000"
  ctx.strokeRect(0,0, width, height)
  
  return canvas.toDataURL()
} 