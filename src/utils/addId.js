import { v4 as uuid } from 'uuid'

export default function (item, idPropName = 'id') {
  if (item[idPropName]) {
    return item
  }
  return {
    ...item,
    [idPropName]: uuid(),
  }
}