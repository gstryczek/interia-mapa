import { createSelector } from 'reselect'
import { find } from 'lodash'

export const getShops = state => state.shops
export const getSelectedShopId = state => state.selectedShop

export const getSelectedShop = createSelector(
  getShops,
  getSelectedShopId,
  (shops, id) => find(shops, shop => shop.id === id)
)
