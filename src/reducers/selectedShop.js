export const SELECT = 'selectedShop/SELECT'
export const DESELECT = 'selectedShop/DESELECT'

export default function selectedShopReducer(state = null, action){
  switch(action.type) {
    case SELECT:
      return action.id
    case DESELECT:
      return null
    default:
      return state
  }
}

export const selectShop = (id) => ({
  type: SELECT,
  id,
})

export const deselectShop = () => ({
  type: DESELECT,
})

