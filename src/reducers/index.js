import { combineReducers } from 'redux'
import shops from './shops'
import selectedShop from './selectedShop'

export default combineReducers({
  shops,
  selectedShop,
})
