import { shopsURL } from '../constants'
import addId from '../utils/addId'

export const LOAD = 'shops/LOAD'

export default function shopsReducer(state = [], action = {}) {
  switch (action.type) {
    case LOAD:
      return action.shops
    default:
      return state
  }
}

export function loadShops(shops) {
  return {
    type: LOAD,
    shops,
  }
}

export function fetchShops() {
  return dispatch => fetch(shopsURL)
    .then(response => response.json())
    .then((shops) => {
      const shopsWithId = shops.map(shop => addId(shop))
      dispatch(loadShops(shopsWithId))
    })
}
