**Zadanie**

Stwórz interaktywną mapę Google z listą sklepów i godzinami ich otwarcia.

Json z danymi do wykorzystania:
- https://gist.github.com/kubalasecki/7b8d731b7f0a12387a96bc2c39037536

Projekt powinien:
- wykorzystywać bibliotekę React do rysowania swojego UI
- przechowywać dane w globalnym store (najlepiej Redux)
- ładować dane mapy asynchronicznie (json z danymi)
- być napisany w ES6/ES7 a zarazem wspierać nieco starsze przeglądarki (IE 11)
- kod projektu hostowany na Github/Bitbucket
- wersja do wglądu udostępniona na Heroku

Zalecenia UI:
- po wybraniu sklepu z listy mapa powinna się na niego automatycznie centrować obok listy sklepów powinno wyświetlić się jego logo oraz godziny otwarcia
- ostylowanie projektu powinno być zintegrowane z React
- dymki sklepów (ich znaczniki na mapie) powinny posiadać loga sieci a ich kliknięcie powinno działać analogicznie do kliknięcia sklepu na liście
- jeśli zajdzie sytuacja braku logotypu sieci milew wdziane rozwiązanie obsługujące ta ką sytuację

Mile widziane:
- dostosowanie komponentu do server-side renderingu
- podpięcie eventów trackujących google analytics w celu zbierania informacji o zachowaniu użytkownika przy poszczególnych akcjach i obsługi interaktywnej mapy
- małe commity na Githubie/Bitbuckecie pokazujące przebieg tworzenia zadania i pozwalające zrozumieć zamysł autora i rozwój koncepcji

Nie dozwolone:
- korzystanie z gotowych bibliotek wrappujących komponent mapy i znaczników. 

Przygotowany powinien być własny komponent obudowujący tworzenie customowych markerów i inicjujący mapę.
